#!/usr/bin/env bash
set -eo pipefail

KEYTAB_LOCATION=./files/keytab-acclog-decrypted
if [[ -z $JENKINS_ENV ]]; then
  VAULT_PASS=.pass
fi

cp ./files/keytab-acclog-encrypted $KEYTAB_LOCATION
ansible-vault decrypt --vault-pass $VAULT_PASS $KEYTAB_LOCATION

INSTALLATION_LOCATION=/opt/acc-py/base/2020.11
source $INSTALLATION_LOCATION/setup.sh

set -u

VENV_LOCATION=./venv
rm -rf $VENV_LOCATION
python -m venv $VENV_LOCATION

source $VENV_LOCATION/bin/activate
pip install --editable '.[test]'

pytest ./tests --principal=$USER --keytab=$KEYTAB_LOCATION
