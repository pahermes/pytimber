import pytest


@pytest.mark.integration
class TestIntegration:

    @pytest.mark.core
    @pytest.mark.parametrize(
        "nodes_pattern, parent_node, child_node, count_children",
        [
            ("/CMS/Beam Condition%", "/CMS/Beam Condition Monitors", "/CMS/Beam Condition Monitors/BCM1 Left", 5)
        ],
    )
    def test_get_children_for_hierarchies_as_pattern(self, ldb, nodes_pattern, parent_node, child_node, count_children):
        result = ldb.getChildrenForHierarchies(nodes_pattern)

        assert child_node in result[parent_node]
        assert len(result) == count_children

    @pytest.mark.core
    @pytest.mark.parametrize(
        "nodes_list, count_children",
        [
            (["/CMS/Beam Condition Monitors", "/CMS/Radiation"], 5)
        ],
    )
    def test_get_children_for_hierarchies_as_list(self, ldb, nodes_list, count_children):
        result = ldb.getChildrenForHierarchies(nodes_list)

        total_children = 0
        for node in result:
            total_children += len(result[node])

        assert total_children == count_children

    @pytest.mark.core
    @pytest.mark.parametrize(
        "nodes_pattern, nodepath, variable, count",
        [
            ("%ALICE/Temperatures", "/LHC/ALICE/Temperatures", "ALICE.BEAMPIPE.SENSOR00:TEMP", 14),
            ("/SPS/Kickers/Injection/%", "/SPS/Kickers/Injection/Magnets", "MKP.11931:TEMPERATURE.1", 36),
        ],
    )
    def test_get_vars_for_hierarchies_as_pattern(self, ldb, nodes_pattern, nodepath, variable, count):
        result = ldb.getVariablesForHierarchies(nodes_pattern)

        variables = result[nodepath]
        assert variable in variables
        assert len(variables) >= count

    @pytest.mark.core
    @pytest.mark.parametrize(
        "nodes_list, count_variables",
        [
            (["/LHC/ALICE/Temperatures", "/SPS/Kickers/Injection/Magnets"], 62),
        ],
    )
    def test_get_vars_for_hierarchies_as_list(self, ldb, nodes_list, count_variables):
        result = ldb.getVariablesForHierarchies(nodes_list)

        total_variables = 0
        for node in result:
            total_variables += len(result[node])

        assert total_variables == count_variables

    @pytest.mark.core
    @pytest.mark.parametrize(
        "variables_pattern, variable, nodepath, count",
        [
            ("BLMXD.1L5.1F1L4%", "BLMXD.1L5.1F1L4:THRESH_RS02", "/CMS/Beam Condition Monitors/BCM1 Left", 2)
        ],
    )
    def test_get_hierarchies_for_vars_as_pattern(self, ldb, variables_pattern, variable, nodepath, count):
        result = ldb.getHierarchiesForVariables(variables_pattern)

        hierarchies = result[variable]
        assert nodepath in hierarchies
        assert len(hierarchies) == count

    @pytest.mark.core
    @pytest.mark.parametrize(
        "variables_list, count_hierarchies",
        [
            (["BLMXD.1L5.1F1L4:THRESH_RS02", "BLMXD.1L5.1F1L4:LOSS_RS05"], 4),
        ],
    )
    def test_get_hierarchies_for_vars_as_list(self, ldb, variables_list, count_hierarchies):
        result = ldb.getHierarchiesForVariables(variables_list)

        total_hierarchies = 0
        for node in result:
            total_hierarchies += len(result[node])

        assert total_hierarchies == count_hierarchies
