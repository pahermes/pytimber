import pytimber
import pytest

@pytest.mark.core
def test_init_default(request):
    ldb = pytimber.LoggingDB(kerberosprincipal=request.config.getoption('--principal'),
                             kerberoskeytab=request.config.getoption('--keytab'))
    assert ldb is not None, "ldb must not be null"


def test_init_flavour(request):
    ldb = pytimber.LoggingDB(sparkconf="large",
                             kerberosprincipal=request.config.getoption('--principal'),
                             kerberoskeytab=request.config.getoption('--keytab'))
    assert ldb is not None, "ldb must not be null"


def test_init_flavour_and_dict(request):
    ldb = pytimber.LoggingDB(sparkconf="large",
                             sparkprops={"spark.ui.enabled":"true"},
                             kerberosprincipal=request.config.getoption('--principal'),
                             kerberoskeytab=request.config.getoption('--keytab'))
    assert ldb is not None, "ldb must not be null"














