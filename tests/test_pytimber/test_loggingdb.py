import datetime
import pytest
import jpype
import numpy as np

from pytimber.fundamentals import Fundamentals


@pytest.mark.unit
class TestUnit:
    def test_timestamp(self, ldb):
        import time

        now = time.time()
        ts_now = ldb.toTimestamp(now)
        now2 = ldb.fromTimestamp(ts_now, unixtime=True)
        dt_now2 = ldb.fromTimestamp(ts_now, unixtime=False)
        assert now == now2
        assert (
            str(ts_now)[:24] == dt_now2.strftime("%Y-%m-%d %H:%M:%S.%f")[:24]
        )

        time_str = "2015-10-12 12:12:32.453255123"
        ta = ldb.toTimestamp(time_str)
        formatter = jpype.java.time.format.DateTimeFormatter.ISO_LOCAL_DATE_TIME
        assert formatter.format(ta.toLocalDateTime()) == "2015-10-12T12:12:32.453255123"
        unix = ldb.fromTimestamp(ta, unixtime=True)
        assert unix == 1444644752.4532552
        assert (
            time.strftime("%b %-d, %Y %-I:%M:%S %p", time.localtime(unix))
            == "Oct 12, 2015 12:12:32 PM"
        )

    def test_timestamp_with_datetime64(self, ldb):
        d = np.datetime64('2021-11-15T12:20:15.123456789')
        ts = ldb.toTimestamp(d)

        assert ts.toInstant().toString() == "2021-11-15T12:20:15.123456789Z"

    @staticmethod
    def _search(ldb, pattern):
        return ldb.search(pattern)

    @pytest.mark.core
    def test_should_search(self, monkeypatch, ldb):
        def mockreturn(pattern):
            _Variable = jpype.JPackage(
                "cern"
            ).nxcals.api.backport.domain.core.metadata.Variable

            var_list = []
            for i in [1, 2, 3]:
                var_list.append(
                    _Variable.builder()
                    .variableName(pattern.replace("%", str(i)))
                    .build()
                    .toString()
                )
            return var_list

        monkeypatch.setattr(ldb, "search", mockreturn)
        variable_list = TestUnit._search(ldb, "VARIABLE%")

        assert "VARIABLE1" in variable_list

    @pytest.mark.core
    def test_should_get_metadata__skip_non_context_var(self, ldb):
        var = 'ATLAS:LUMI_TOT_INST'
        context_var = 'LHC.BLMI:CABLE_CONNECTED'

        result = ldb.getMetaData([var, context_var])
        assert var not in result.keys()
        assert context_var in result.keys()


@pytest.mark.integration
class TestIntegration:
    @pytest.mark.core
    @pytest.mark.parametrize(
        "pattern, variable_name, count", [("HX:BETA%", "HX:BETASTAR_IP1", 4)]
    )
    def test_search(self, ldb, pattern, variable_name, count):
        variables = ldb.search(pattern)
        assert variable_name in variables
        assert len(variables) == count

    class TestGet:
        @pytest.mark.core
        @pytest.mark.parametrize(
            "t1, t2, variable, count, ts, value",
            [
                (
                    "2015-05-13 12:00:00.000",
                    "2015-05-15 00:00:00.000",
                    "HX:FILLN",
                    6,
                    1431523684.764,
                    3715.0,
                )
            ],
        )
        def test_get_simple(self, ldb, t1, t2, variable, count, ts, value):
            data = ldb.get(variable, t1, t2)

            t, v = data[variable]
            assert len(t) == count
            assert len(v) == count

            assert t[0] == ts
            assert v[0] == value

        @pytest.mark.core
        @pytest.mark.parametrize(
            "t1, t2, vars, count",
            [
                (
                    1668359400, 1668364200,
                    ['ACAB1_05L4_LTMIN.POSST', 'QRLHA_05L4_PV890DO.POSST', 'NON_EXISTENT'],
                    2
                )
            ],
        )
        def test_get_vars_with_empty_dataset(self, ldb, t1, t2, vars, count):

            data = ldb.get(vars, t1, t2)
            assert len(data) == count


        import datetime

        @pytest.mark.parametrize(
            "t1, t2, variable, dt",
            [
                (
                    "2015-05-13 12:00:00.000",
                    "2015-05-15 00:00:00.000",
                    "HX:FILLN",
                    datetime.datetime(2015, 5, 13, 15, 28, 4, 764000),
                )
            ],
        )
        def test_get_unixtime(self, ldb, t1, t2, variable, dt):
            data = ldb.get(variable, t1, t2, unixtime=False)
            t, v = data[variable]

            assert t[0] == dt

        @pytest.mark.parametrize(
            "t1, t2, variable, count",
            [
                (
                    "2015-05-13 12:00:00.000",
                    "2015-05-13 12:00:01.000",
                    "LHC.BQBBQ.CONTINUOUS_HS.B1:ACQ_DATA_H",
                    4096,
                ),
                (
                    "2021-10-22 00:00:00.000",
                    "2021-10-22 00:00:05.000",
                    "LHC.BLMI:CONNECTED_TO_BIS",
                    3760
                 )
            ]
        )
        def test_get_vectornumeric(self, ldb, t1, t2, variable, count):
            data = ldb.get(variable, t1, t2)

            t, v = data[variable]

            for vv in v:
                assert len(vv) == count

        @pytest.mark.parametrize(
            "t1, t2, variable, count_matrices, first_element",
            [
                (
                    "2021-04-12 21:00:00.000",
                    "2021-04-12 23:59:00.000",
                    "LBE.BWS.0025:DATA_H",
                    4,
                    -22
                )
            ],
        )
        def test_get_matrixnumeric(self, ldb, t1, t2, variable, count_matrices, first_element):
            data = ldb.get(variable, t1, t2)
            t, v = data[variable]

            assert v[0][0][0] == first_element
            assert len(v) == count_matrices

    @pytest.mark.parametrize(
        "pattern_or_list, start_time, end_time",
        [
            (
                "%:LUMI_TOT_INST",
                "2018-10-17 15:00:00.000",
                "2018-10-17 15:05:00.000",
            )
        ],
    )
    def test_get_aligned(self, ldb, pattern_or_list, start_time, end_time):
        # TODO
        assert True

    @pytest.mark.parametrize(
        "pattern, variable, description",
        [
            (
                "%:LUMI_TOT_INST",
                "ATLAS:LUMI_TOT_INST",
                "ATLAS: Total instantaneous luminosity summed over all bunches",
            )
        ],
    )
    def test_getdescription(self, ldb, pattern, variable, description):
        descriptions = ldb.getDescription(pattern)

        assert descriptions[variable] == description

    class TestFundamentals:
        @pytest.mark.parametrize(
            "variable, t1, t2, fundamental, value",
            [
                (
                    "CPS.TGM:USER",
                    "2015-05-15 12:00:00.000",
                    "2015-05-15 12:01:00.000",
                    "CPS:%:SFTPRO%",
                    "SFTPRO2",
                )
            ],
        )
        def test_filter_by_fundamentals(
            self, ldb, variable, t1, t2, fundamental, value
        ):
            t, v = ldb.getVariable(
                variable, t1, t2, fundamental=fundamental
            )
            assert v[0] == value

        @pytest.mark.parametrize(
            "variable, t1, t2, fundamental, count",
            [
                ("PR.BLMIB00.UCAP:Acquisition:lossBeamPresence",
                 "2022-07-01 02:00:00.000", "2022-07-01 03:00:00.000",
                 Fundamentals("CPS:ADO_22:AD", ["TT2_FTA"], []), 31),
                ("PR.BLMIB00.UCAP:Acquisition:lossBeamPresence",
                 "2022-07-01 02:00:00.000", "2022-07-01 03:00:00.000",
                 Fundamentals("CPS:ADO_22:AD", [], ["TT2_D3"]), 31),
                ("PR.BLMIB00.UCAP:Acquisition:lossBeamPresence",
                 "2022-07-01 02:00:00.000", "2022-07-01 03:00:00.000",
                 Fundamentals("CPS:ADO_22:AD", ["TT2_D3"], []), 93),
            ],
        )
        def test_filter_by_fundamentals_with_destination(
            self, ldb, variable, t1, t2, fundamental, count
        ):
            result = ldb.get(variable, t1, t2, fundamental)
            assert len(result[variable][0]) == count

        @pytest.mark.parametrize(
            "variable, t1, t2, fundamental",
            [
                (
                    "FTN.BCT.477:TOTAL_INTENSITY",
                    "2022-06-01 13:00:00.000",
                    "2022-06-01 13:15:15.000",
                    "CPS:EAST_T9_22:UNKNOWN",
                )
            ],
        )
        def test_filter_by_empty_fundamentals(self, ldb, variable, t1, t2, fundamental):
            assert ldb.get(variable, t1, t2, fundamental) == {}

        @pytest.mark.parametrize(
            "t1, t2, pattern, value",
            [
                (
                    "2018-10-17 15:00:00.000",
                    "2018-10-17 15:05:00.000",
                    "CPS:%:%",
                    "CPS:AD:AD",
                )
            ],
        )
        def test_get_fundamentals(self, ldb, t1, t2, pattern, value):
            fundamentals = ldb.getFundamentals(t1, t2, pattern)
            assert sorted(fundamentals)[0] == value

        @pytest.mark.parametrize(
            "t1, t2, pattern, value, length",
            [
                (
                    "2018-10-17 05:15:00.000",
                    "2018-10-17 05:16:00.000",
                    "CPS:%:%",
                    "CPS:EAST_NORTH:EAST2",
                    5,
                )
            ],
        )
        def test_search_fundamental(
            self, ldb, t1, t2, pattern, value, length
        ):
            fundamentals = ldb.searchFundamental(pattern, t1, t2)
            assert sorted(fundamentals)[0] == value
            assert len(fundamentals) == length

    class TestLHC:
        @pytest.mark.parametrize(
            "start_time, end_time, mode1, mode2, int_cnt, first_fill, int_start, int_end",
            [
                (
                    "2018-09-23 10:00:00.000",
                    "2018-09-26 00:00:00.000",
                    "SETUP",
                    "STABLE",
                    4,
                    7212,
                    "2018-09-23 16:15:03",
                    "2018-09-23 23:52:36",
                )
            ],
        )
        def test_get_intervals(
            self,
            ldb,
            start_time,
            end_time,
            mode1,
            mode2,
            int_cnt,
            first_fill,
            int_start,
            int_end,
        ):
            intervals = ldb.getIntervalsByLHCModes(
                ldb.toTimestamp(start_time),
                ldb.toTimestamp(end_time),
                mode1,
                mode2,
            )
            assert len(intervals) == int_cnt
            assert intervals[0][0] == first_fill
            assert (
                datetime.datetime.utcfromtimestamp(intervals[0][1]).strftime(
                    "%Y-%m-%d %H:%M:%S"
                )
                == int_start
            )
            assert (
                datetime.datetime.utcfromtimestamp(intervals[0][2]).strftime(
                    "%Y-%m-%d %H:%M:%S"
                )
                == int_end
            )

        @pytest.mark.parametrize(
            "fill_nr, fill_start_time, fill_last_mode, mode_start_time",
            [(7218, "2018-09-24 22:38:03", "RAMPDOWN", "2018-09-25 14:12:45")],
        )
        def test_get_fill_data(
            self,
            ldb,
            fill_nr,
            fill_start_time,
            fill_last_mode,
            mode_start_time,
        ):
            fill_data = ldb.getLHCFillData(fill_nr)

            last = len(fill_data["beamModes"]) - 1
            assert (
                datetime.datetime.utcfromtimestamp(
                    fill_data["startTime"]
                ).strftime("%Y-%m-%d %H:%M:%S")
                == fill_start_time
            )
            assert fill_data["beamModes"][last]["mode"] == fill_last_mode
            assert (
                datetime.datetime.utcfromtimestamp(
                    fill_data["beamModes"][last]["startTime"]
                ).strftime("%Y-%m-%d %H:%M:%S")
                == mode_start_time
            )

        @pytest.mark.parametrize(
            "start_time, end_time, beam_modes, fills_cnt, first_fill",
            [
                (
                    "2018-09-28 00:00:00.000",
                    "2018-10-02 00:00:00.000",
                    "STABLE",
                    5,
                    7234,
                )
            ],
        )
        def test_get_fill_by_time(
            self,
            ldb,
            start_time,
            end_time,
            beam_modes,
            fills_cnt,
            first_fill,
        ):
            fills = ldb.getLHCFillsByTime(start_time, end_time, beam_modes)
            assert len(fills) == fills_cnt
            assert fills[0]["fillNumber"] == first_fill

    @pytest.mark.parametrize(
        "variable, first_element, length",
        [
            ('LHC.BLMI:CABLE_CONNECTED', 'BLMQI.01R1.B2I30_MQXA', 30),
            ('LHC.BLMI:LOSS_RS09', 'BLMQI.01R1.B2I30_MQXA', 30)
        ]
    )
    def test_get_metadata(self, ldb, variable, first_element, length):
        result = ldb.getMetaData(variable)

        assert result[variable][1][0][0] == first_element
        assert len(result[variable][0]) >= length
        assert len(result[variable][1]) >= length

    import numpy as np

    @pytest.mark.parametrize(
        "variable, t1, t2, scale_interval, scale_algorithm, scale_size, result",
        [
            (
                "MSC01.ZT8.107:COUNTS",
                "2015-05-15 12:00:00.000",
                "2015-05-15 15:00:00.000",
                "HOUR",
                "SUM",
                "1",
                np.array([1174144.0, 1172213.0, 1152831.0]),
            )
        ],
    )
    def test_getscaled(
        self,
        ldb,
        variable,
        t1,
        t2,
        scale_interval,
        scale_algorithm,
        scale_size,
        result,
    ):
        data = ldb.getScaled(
            variable,
            t1,
            t2,
            scaleInterval=scale_interval,
            scaleAlgorithm=scale_algorithm,
            scaleSize=scale_size,
        )

        t, v = data[variable]

        assert (v[:4] - result).sum() == 0

    @pytest.mark.parametrize(
        "variables , t1, t2, scale_interval, scale_algorithm, scale_size, result",
        [
            (
                ["IP.NSRCGEN:BIASDISCAQNI", "IP.NSRCGEN:BIASDISCAQNV"],
                "2018-12-10 00:10:05.000",
                "2018-12-10 00:10:45.000",
                "SECOND",
                "SUM",
                "10",
                np.array(
                    [
                        -7.630000114440918,
                        -12.130000114440918,
                        -15.59000015258789,
                    ]
                ),
            )
        ],
    )
    def test_getscaled_for_list(
        self,
        ldb,
        variables,
        t1,
        t2,
        scale_interval,
        scale_algorithm,
        scale_size,
        result,
    ):
        data = ldb.getScaled(
            variables,
            t1,
            t2,
            scaleInterval=scale_interval,
            scaleAlgorithm=scale_algorithm,
            scaleSize=scale_size,
        )

        t, v = data[variables[0]]
        assert self.is_close((v[1:4] - result).sum(), 0, 6)

    @staticmethod
    def is_close(float_a, float_b, prec):
        if round(float_a, prec) == round(float_b, prec):
            return True
        return False

    @pytest.mark.parametrize(
        "variable, t1, t2, min_timestamp, std_deviation",
        [
            (
                "LHC.BOFSU:EIGEN_FREQ_2_B1",
                "2016-03-01 00:00:00.000",
                "2016-04-03 00:00:00.000",
                pytest.approx(1457962796.972),
                0.00401594,
            )
        ],
    )
    def test_getstats(
        self, ldb, variable, t1, t2, min_timestamp, std_deviation
    ):
        stat = ldb.getStats(variable, t1, t2)[variable]

        assert stat.MinTstamp == min_timestamp
        assert self.is_close(stat.StandardDeviationValue, std_deviation, 8)

    @pytest.mark.parametrize(
        "pattern, variable, unit",
        [("%:LUMI_TOT_INST", "ATLAS:LUMI_TOT_INST", "Hz/ub")],
    )
    def test_getunit(self, ldb, pattern, variable, unit):
        units = ldb.getUnit(pattern)
        assert units[variable] == unit

    class TestVariable:
        @pytest.mark.parametrize(
            "variable, t1, t2, tstamp_count, value_count, tstamp, value",
            [
                (
                    "HX:FILLN",
                    "2015-05-13 12:00:00.000",
                    "2015-05-15 00:00:00.000",
                    6,
                    6,
                    1431523684.764,
                    3715.0,
                )
            ],
        )
        def test_getvariable(
            self,
            ldb,
            variable,
            t1,
            t2,
            tstamp_count,
            value_count,
            tstamp,
            value,
        ):
            t, v = ldb.getVariable(variable, t1, t2)

            assert len(t) == tstamp_count
            assert len(v) == value_count

            assert t[0] == tstamp
            assert v[0] == value

        @pytest.mark.parametrize("pattern, var_cnt", [("ALICE:LUMI%TOT%INST", 1)])
        def test_get_variables_with_name_like_pattern(
            self, ldb, pattern, var_cnt
        ):
            # Currently Java VariableSet object returned
            variables = ldb.getVariableSet(pattern)
            assert len(variables) == var_cnt

        @pytest.mark.parametrize(
            "varlist", [(["ALICE:BUNCH_LUMI_INST", "ALICE:LUMI_TOT_INST"])]
        )
        def test_get_variables_with_name_in_list(self, ldb, varlist):
            # Currently Java VariableSet object returned
            variables = ldb.getVariableSet(varlist)
            assert len(variables) == 2

        @pytest.mark.parametrize(
            "variable, t1, t2, idx1, idx2, value",
            [
                (
                    "HIE-BCAM-T3M04:RAWMEAS#SEQ_ID",
                    "2018-03-26 00:00:00.000",
                    "2018-03-26 06:00:00.000",
                    0,
                    2,
                    "SEQ_T3M04_FRONT_03",
                ),
                (
                    "LHC.BOFSU:BPM_NAMES_H",
                    "2016-03-28 00:00:00.000",
                    "2016-03-28 23:59:59.999",
                    0,
                    123,
                    "BPM.16L3.B1"
                )
            ],
        )
        def test_get_vectorstring(
            self, ldb, variable, t1, t2, idx1, idx2, value
        ):
            t, v = ldb.getVariable(variable, t1, t2)
            assert v[idx1][idx2] == value

    class TestMetadata:

        @pytest.mark.parametrize(
            "pattern_or_list, variable, system",
            [
                (
                    ["UAT2_UMI1_171RS.OFFST", "DQQDS.B15R3.RQF.A34:U_REF_N1"],
                    "UAT2_UMI1_171RS.OFFST",
                    "WINCCOA"
                ),
                (
                    "DQQDS.B15R3.RQF.A34:%",
                    "DQQDS.B15R3.RQF.A34:U_REF_N1",
                    "CMW"
                ),
            ],
        )
        def test_variables_origin(self, ldb, pattern_or_list, variable, system):
            assert ldb.getVariablesOrigin(pattern_or_list)[variable] == system
