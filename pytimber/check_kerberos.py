def check_kerberos():
    import subprocess

    kerb_list = subprocess.run(["klist"], stdout=subprocess.PIPE)
    ret = subprocess.run(['grep', 'krbtgt/CERN.CH@CERN.CH'], input=kerb_list.stdout, stdout=subprocess.PIPE)

    if len(ret.stdout) == 0:
        import getpass

        user = getpass.getuser()
        mypass = getpass.getpass(f"kinit: type the password for `{user}`:")
        subprocess.run(["kinit"], input=mypass.encode())
