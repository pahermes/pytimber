from typing import Optional


class SnapshotSearchCriteria:
    def __init__(self, fundamental_pattern: Optional[str] = None, start_time: Optional[str] = None, end_time: Optional[str] = None):
        self._fundamental_pattern: Optional[str] = fundamental_pattern
        self._start_time: Optional[str] = start_time
        self._end_time: Optional[str] = end_time

    @property
    def start_time(self) -> Optional[str]:
        return self._start_time

    @property
    def end_time(self) -> Optional[str]:
        return self._end_time

    @property
    def fundamental_pattern(self) -> Optional[str]:
        return self._fundamental_pattern

    @fundamental_pattern.setter
    def fundamental_pattern(self, value: Optional[str]):
        self._fundamental_pattern = value

    @start_time.setter
    def start_time(self, value: Optional[str]):
        self._start_time = value

    @end_time.setter
    def end_time(self, value: Optional[str]):
        self._end_time = value
