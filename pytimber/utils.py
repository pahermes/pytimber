def _adjust_timestamp_format(ts: str) -> str:
    return ts.replace("T", " ").replace("Z", "")