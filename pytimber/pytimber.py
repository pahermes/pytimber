"""
PyTimber -- A Python wrapping of NXCALS API

Copyright (c) CERN 2015-2021

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
of the Software, and to permit persons to whom the Software is furnished to do
so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

Authors:
    R. De Maria     <riccardo.de.maria@cern.ch>
    T. Levens       <tom.levens@cern.ch>
    C. Hernalsteens <cedric.hernalsteens@cern.ch>
    M. Betz         <michael.betz@cern.ch>
    M. Fitterer     <mfittere@fnal.gov>
    R. Castellotti  <riccardo.castellotti@cern.ch>
    L. Coyle        <loic.coyle@epfl.ch>
    P. Sowinski     <piotr.sowinski@cern.ch>
"""
import json
import warnings
from collections import namedtuple
import datetime
import logging
import time
from typing import Union, List, Tuple, Dict, Set, Any, Optional
from dateutil.relativedelta import relativedelta

import cmmnbuild_dep_manager
import jpype
import numpy as np
import os

from pytz import timezone

from .check_kerberos import check_kerberos
from .fundamentals import Fundamentals
from .snapshots import SnapshotSearchCriteria
from .utils import _adjust_timestamp_format

Stat = namedtuple(
    "Stat",
    [
        "MinTstamp",
        "MaxTstamp",
        "ValueCount",
        "MinValue",
        "MaxValue",
        "AvgValue",
        "StandardDeviationValue",
    ],
)

DEFAULT_HIERARCHY_NXCALS_SYSTEM = 'CERN'


def _Timestamp2float(ts):
    return int(ts.getTime() / 1000.0) + ts.getNanos() / 1e9


# Documentation NXCALS API
# http://nxcals-docs.web.cern.ch/current/java-docs/index.html


class LoggingDB(object):
    def __init__(
        self,
        appid="PyTimber3",
        clientid="PYTIMBER3",
        source="nxcals",
        loglevel=None,
        sparkconf=None,
        sparkprops=None,
        kerberosprincipal=None,
        kerberoskeytab=None,
        data_location="pro",
        spark_session=None
    ):
        """
        High level interface to NXCALS service

          sparkconf: None for Spark local mode, 'small', 'medium' and 'large' for yarn mode
          data_location: origin of data: 'pro' or 'testbed' Hadoop cluster
        """
        # Configure logging
        logging.basicConfig()
        self._log = logging.getLogger(__name__)
        if loglevel is not None:
            self._log.setLevel(loglevel)

        # Start JVM
        mgr = cmmnbuild_dep_manager.Manager("pytimber")
        mgr.start_jpype_jvm()
        self._mgr = mgr

        self._org = jpype.JPackage("org")
        self._cern = jpype.JPackage("cern")

        # log4j config
        conf =  self._org.apache.logging.log4j.core.config.Configurator
        null_conf =  self._org.apache.logging.log4j.core.config.NullConfiguration()
        conf.initialize(null_conf)

        self._source = source

        if self._source != "nxcals":
            raise ValueError("Currently only \"nxcals\" is supported as source for data.")

        self._System = jpype.java.lang.System

        if kerberosprincipal is not None and kerberoskeytab is not None:
            self._System.setProperty(
                "kerberos.principal",
                kerberosprincipal,
            )
            self._System.setProperty(
                "kerberos.keytab",
                kerberoskeytab,
            )
            KerberosRelogin = self._cern.nxcals.api.security.KerberosRelogin(kerberosprincipal, kerberoskeytab, True)
            KerberosRelogin.start()
        else:
            check_kerberos()

        self._System.setProperty(
            "service.url",
            "https://cs-ccr-nxcals6.cern.ch:19093,https://cs-ccr-nxcals7.cern.ch:19093,https://cs-ccr-nxcals8.cern.ch:19093",
        )

        ServiceBuilder = self._cern.nxcals.api.backport.client.service.ServiceBuilder

        self._config = self._cern.nxcals.api.config
        if sparkconf is not None:
            self._custom_config = self._cern.nxcals.api.custom.config
            self._utils = self._cern.nxcals.api.utils

            configuration_mapper = {
                "small": self._custom_config.SparkSessionFlavor.SMALL,
                "medium": self._custom_config.SparkSessionFlavor.MEDIUM,
                "large": self._custom_config.SparkSessionFlavor.LARGE
            }

            if sparkconf in configuration_mapper:
                spark_flavor = configuration_mapper.get(sparkconf)
            else:
                raise ValueError(f"{sparkconf} is not a valid SparkSessionFlavor name")

            spark_properties = self._config.SparkProperties.remoteSessionProperties(appid, spark_flavor, data_location)

            if sparkprops is not None:
                spark_properties.setProperties(sparkprops)

            server_hostname = os.environ.get('SERVER_HOSTNAME')

            if server_hostname is not None:
                spark_properties.setProperty('spark.driver.host', server_hostname)
            self._spark_session = self._utils.SparkUtils.createSparkSession(spark_properties)

            builder = ServiceBuilder.getInstance(self._spark_session)
        else:
            if spark_session:
                if not spark_session.conf.get("spark.app.name"):
                    spark_session.conf.set("spark.app.name", appid)
                builder = ServiceBuilder.getInstance(spark_session)
            else:
                builder = ServiceBuilder.getInstance(self._config.SparkProperties.defaults(appid))

        self._nxcals_api = self._cern.nxcals.api
        self._nxcals_data_api = self._nxcals_api.extraction.data
        self._nxcals_metadata_api = self._nxcals_api.extraction.metadata
        self._nxcals_custom_api = self._nxcals_api.custom
        self._nxcals_backport_api = self._nxcals_api.backport

        self._md = builder.createMetaService()
        self._ts = builder.createTimeseriesService()
        self._FillService = builder.createLHCFillService()
        self._VariableDataType = self._nxcals_backport_api.domain.core.constants.VariableDataType
        self._BeamModeValue = self._nxcals_backport_api.domain.core.constants.BeamModeValue
        self._VariableSet = self._nxcals_backport_api.domain.core.metadata.VariableSet

        self._hierarchyService = self._nxcals_metadata_api.ServiceClientFactory.createHierarchyService()
        self._variableService = self._nxcals_metadata_api.ServiceClientFactory.createVariableService()
        self._groupService = self._nxcals_metadata_api.ServiceClientFactory.createGroupService()

    def toTimestamp(self, t):
        Timestamp = jpype.java.sql.Timestamp
        if isinstance(t, str):
            return Timestamp.valueOf(t)
        elif isinstance(t, datetime.datetime):
            return Timestamp.valueOf(t.strftime("%Y-%m-%d %H:%M:%S.%f"))
        elif isinstance(t, np.datetime64):
            ts = Timestamp(int(t.item() / 1000000))
            nanos = t.item() % 1000000000
            ts.setNanos(nanos)
            return ts
        elif t is None:
            return None
        elif isinstance(t, Timestamp):
            return t
        else:
            ts = Timestamp(int(t * 1000))
            sec = int(t)
            nanos = int((t - sec) * 1e9)
            ts.setNanos(nanos)
            return ts

    def fromTimestamp(self, ts, unixtime):
        if ts is None:
            return None
        else:
            t = _Timestamp2float(ts)
            if unixtime:
                return t
            else:
                # TODO: This isn't necessarily doing the right thing as it
                #  produces a timezone ignorant date-time object.
                return datetime.datetime.fromtimestamp(t)

    def toStringList(self, myArray):
        myList = jpype.java.util.ArrayList()
        for s in myArray:
            myList.add(s)
        return myList

    def toTimescale(self, timescale_list):
        Timescale = self._cern.nxcals.api.backport.domain.core.constants.TimescalingProperties

        try:
            timescale_str = "_".join(timescale_list)
            return Timescale.valueOf(timescale_str)
        except Exception as e:
            self._log.warning("exception in timescale:{}".format(e))

    def search(self, pattern):
        """Search for parameter names. Wildcard is '%'."""
        types = self._VariableDataType.ALL
        vl = self._md.getVariablesOfDataTypeWithNameLikePattern(pattern, types)
        return [vv.getVariableName() for vv in vl.getVariables()]

    def getDescription(self, pattern):
        """Get Variable Description from pattern. Wildcard is '%'."""
        return dict(
            [
                (vv.getVariableName(), vv.getDescription())
                for vv in self.getVariableSet(pattern)
            ]
        )

    def getUnit(self, pattern):
        """Get Variable Unit from pattern. Wildcard is '%'."""
        return dict(
            [
                (vv.getVariableName(), vv.getUnit())
                for vv in self.getVariableSet(pattern)
            ]
        )

    def _getFundamentals(self, t1, t2, fundamental_pattern):
        self._log.info(
            f"Querying fundamentals (pattern: {fundamental_pattern})"
        )
        fundamentals = self._md.getFundamentalsInTimeWindowWithNameLikePattern(
            t1, t2, fundamental_pattern
        )
        if fundamentals is None or fundamentals.size() == 0:
            self._log.info("No fundamental found in time window")
        else:
            logfuns = []
            for f in fundamentals.getVariables():  # workaround 0.7.1
                logfuns.append(f.toString())
            self._log.info(
                "List of fundamentals found: {0}".format(", ".join(logfuns))
            )
        return fundamentals

    def getFundamentals(self, t1, t2, fundamental):
        self._log.warning("getFundamentals will be deprecated")
        return self.searchFundamental(fundamental, t1, t2)

    def getVariableSet(self, pattern_or_list):
        """Get a list of variables based on a list of strings or a pattern.
        Wildcard for the pattern is '%'.
        """
        if isinstance(pattern_or_list, str):
            types = self._VariableDataType.ALL
            variables = self._md.getVariablesOfDataTypeWithNameLikePattern(
                pattern_or_list, types
            )
        elif isinstance(pattern_or_list, (list, tuple)):
            variables = self._md.getVariablesWithNameInListofStrings(
                jpype.java.util.Arrays.asList(pattern_or_list)
            )
        else:
            raise ValueError(f"{pattern_or_list} not pattern or list")
        return variables

    def processDataset(self, dataset, datatype, unixtime):

        if type(dataset) is list:
            var = self._cern.nxcals.api.backport.domain.core.metadata.Variable.builder().variableName("dummy").build()
            ds_list = jpype.java.util.ArrayList()

            for data in dataset:
                ds_list.add(data)

            dataset = self._cern.nxcals.api.backport.domain.core.timeseriesdata.SparkTimeseriesDataSet.of(var, ds_list)

        if dataset.isEmpty():
            return np.array([], dtype=float), np.array([], dtype=float)

        timestamps = np.array([_Timestamp2float(datapoint.getStamp()) for datapoint in dataset], dtype=float)

        if not unixtime:
            timestamps = np.array(
                [datetime.datetime.fromtimestamp(t) for t in timestamps]
            )

        if datatype == "NUMERIC":
            try:
                dataset = np.array([None if d.isNullValue() else d.getDoubleValue() for d in dataset])
            except jpype.java.lang.NoSuchMethodException:
                try:
                    dataset = np.array([None if d.isNullValue() else d.getLongValue() for d in dataset])
                except jpype.java.lang.NoSuchMethodException:
                    self._log.warning(
                        "unsupported datatype, returning the java object"
                    )
        elif datatype == "VECTORNUMERIC":
            try:
                dataset = np.array([None if d.isNullValue() else d.getDoubleValues() for d in dataset])
            except jpype.java.lang.NoSuchMethodException:
                try:
                    dataset = np.array([None if d.isNullValue() else d.getLongValues() for d in dataset])
                except jpype.java.lang.NoSuchMethodException:
                    self._log.warning(
                        "unsupported datatype, returning the java object"
                    )
        elif datatype == "TEXTUAL":
            try:
                dataset = np.array([None if d.isNullValue() else d.getVarcharValue() for d in dataset])
            except jpype.java.lang.NoSuchMethodException:
                self._log.warning(
                    "unsupported datatype, returning the java object"
                )
        elif datatype == "VECTORSTRING":
            try:
                dataset = np.array([None if d.isNullValue() else d.getStringValues() for d in dataset])
            except jpype.java.lang.NoSuchMethodException:
                self._log.warning(
                    "unsupported datatype, returning the java object"
                )
        elif datatype == "MATRIXNUMERIC":
            try:
                dataset = [None if d.isNullValue() else np.array(d.getMatrixDoubleValues()) for d in dataset]
            except jpype.java.lang.NoSuchMethodException:
                try:
                    dataset = [None if d.isNullValue() else np.array(d.getMatrixLongValues()) for d in dataset]
                except jpype.java.lang.NoSuchMethodException:
                    self._log.warning(
                       "unsupported datatype, returning the java object"
                    )
            except jpype.java.lang.NoSuchMethodException:
                self._log.warning(
                    "unsupported datatype, returning the java object"
                )
        else:
            self._log.warning(
                "unsupported datatype, returning the java object"
            )

        return timestamps, dataset

    def getAligned(
        self,
        pattern_or_list,
        t1,
        t2,
        fundamental=None,
        master=None,
        unixtime=True,
    ):
        """Get data aligned to a variable"""
        ts1 = self.toTimestamp(t1)
        ts2 = self.toTimestamp(t2)
        out = {}
        master_variable = None

        # Fundamentals
        if fundamental is not None:
            fundamentals = self._getFundamentals(ts1, ts2, fundamental)
            if fundamentals is None or fundamentals.size() == 0:
                return {}

        # Build variable list
        variables = self.getVariableSet(pattern_or_list)

        if master is None:
            if isinstance(pattern_or_list, (list, tuple)):
                master_variable = variables.getVariable(pattern_or_list[0])
            else:
                master_variable = variables.getVariable(0)
        else:
            master_variable = variables.getVariable(master)

        if master_variable is None:
            self._log.warning("Master variable not found.")
            return {}

        master_name = master_variable.toString()

        if len(variables) == 0:
            self._log.warning("No variables found.")
            return {}
        else:
            logvars = []
            for jvar in variables:
                vname = jvar.getVariableName()
                if vname == master_name:
                    logvars.append("{0} (master)".format(vname))
                else:
                    logvars.append(vname)

            self._log.info(
                "List of variables to be queried: {0}".format(
                    ", ".join(logvars)
                )
            )

        # Acquire master dataset
        if fundamental is not None:
            master_ds = self._ts.getDataInTimeWindowFilteredByFundamentals(
                master_variable, ts1, ts2, fundamentals
            )
        else:
            master_ds = self._ts.getDataInTimeWindow(master_variable, ts1, ts2)

        self._log.info(
            "Retrieved {0} values for {1} (master)".format(
                master_ds.size(), master_name
            )
        )

        # Prepare master dataset for output
        out["timestamps"], out[master_name] = self.processDataset(
            master_ds, master_ds.getVariableDataType().toString(), unixtime
        )

        # Acquire aligned data based on master dataset timestamps
        for jvar in variables:
            if jvar.toString() == master_name:
                continue
            start_time = time.time()
            res = self._ts.getDataAlignedToTimestamps(jvar, master_ds)
            self._log.info(
                "Retrieved {0} values for {1}".format(
                    res.size(), jvar.getVariableName()
                )
            )
            self._log.info(
                "{0} seconds for aqn".format(time.time() - start_time)
            )
            out[jvar.getVariableName()] = self.processDataset(
                res, res.getVariableDataType().toString(), unixtime
            )[1]
        return out

    def searchFundamental(self, fundamental, t1, t2=None):
        """Search fundamental"""
        ts1 = self.toTimestamp(t1)
        if t2 is None:
            t2 = time.time()
        ts2 = self.toTimestamp(t2)
        fundamentals = self._getFundamentals(ts1, ts2, fundamental)
        if fundamentals is not None:
            return list(fundamentals.getVariableNames())
        else:
            return []

    def getStats(self, pattern_or_list, t1, t2, unixtime=True):
        ts1 = self.toTimestamp(t1)
        ts2 = self.toTimestamp(t2)

        # Build variable list
        variables = self.getVariableSet(pattern_or_list)
        if len(variables) == 0:
            self._log.warning("No variables found.")
            return {}
        else:
            logvars = []
            for jvar in variables.getVariables():  # workaround 0.7.1
                logvars.append(jvar.toString())
            self._log.info(
                "List of variables to be queried: {0}".format(
                    ", ".join(logvars)
                )
            )

        # Acquire
        data = self._ts.getVariableStatisticsOverMultipleVariablesInTimeWindow(
            variables, ts1, ts2
        )

        out = {}
        for stat in data.getStatisticsList():
            count = stat.getValueCount()
            if count > 0:
                s = Stat(
                    self.fromTimestamp(stat.getMinTstamp(), unixtime),
                    self.fromTimestamp(stat.getMaxTstamp(), unixtime),
                    int(count),
                    stat.getMinValue().doubleValue(),
                    stat.getMaxValue().doubleValue(),
                    stat.getAvgValue().doubleValue(),
                    stat.getStandardDeviationValue().doubleValue(),
                )

                out[stat.getVariableName()] = s

        return out

    #    def getSize(self, pattern_or_list, t1, t2):
    #        ts1 = self.toTimestamp(t1)
    #        ts2 = self.toTimestamp(t2)
    #
    #        # Build variable list
    #        variables = self.getVariableSet(pattern_or_list)
    #        if len(variables) == 0:
    #            log.warning('No variables found.')
    #            return {}
    #        else:
    #            logvars = []
    #            for v in variables:
    #                logvars.append(v)
    #            log.info('List of variables to be queried: {0}'.format(
    #                ', '.join(logvars)))
    #        # Acquire
    #        for v in variables:
    #            return self._ts.getJVMHeapSizeEstimationForDataInTimeWindow(v,ts1,ts2,None,None)

    def _get_timeseries_data_filtered_by_fundamentals(
            self, jvar: Any, ts1: Any, ts2: Optional[Any] = None,
            fundamentals: Optional[Union[str, Fundamentals]] = None) -> Any:

        if isinstance(fundamentals, Fundamentals):
            fundamentals_list = self._getFundamentals(ts1, ts2, fundamentals.fundamental_pattern)
            if fundamentals_list is None or fundamentals_list.size() == 0:
                return {}
                # To be fully compatible with NXCALS we should provide here an empty VariableSet,
                # but that would break up backward compatibility, since we would return unfiltered data
                # instead of empty dictionary as expected by PyTimber users
                #
                # fundamentals_list = self._VariableSet()

            ds_filtered_by_fundamentals = self._ts.getDatasetFilteredByFundamentals(
                jvar, ts1, ts2, fundamentals_list)

            ds_filtered_by_fundamentals = fundamentals.apply_filter(ds_filtered_by_fundamentals)

            res = self._nxcals_backport_api.domain.core.timeseriesdata.SparkTimeseriesDataSet.of(
                jvar, ds_filtered_by_fundamentals)
        else:
            fundamentals_list = self._getFundamentals(ts1, ts2, fundamentals)
            if fundamentals_list is None or fundamentals_list.size() == 0:
                return {}
                # See the comments above
                # fundamentals_list = self._VariableSet()

            res = self._ts.getDataInTimeWindowFilteredByFundamentals(jvar, ts1, ts2, fundamentals_list)

        return res

    def get(
        self, pattern_or_list: Union[str, List[str], Tuple[str]],
            t1: Union['str', datetime.datetime, np.datetime64],
            t2: Optional[Union['str', datetime.datetime, np.datetime64]] = None,
            fundamentals: Optional[Union[str, Fundamentals]] = None, unixtime: Optional[bool] = True
    ) -> Dict[str, Union[Tuple[np.ndarray, np.ndarray], Any]]:
        """Query the database for a list of variables or for variables whose
        name matches a pattern (string) in a time window from t1 to t2.

        If t2 is missing, None, "last", the last data point before t1 is given
        If t2 is "next", the first data point after t1 is given.

        If neither pattern nor Fundamentals objects is provided for the fundamental all the data is returned.

        If a fundamental value is provided, the end of the time window has to
        be explicitly provided.
        """

        ts1 = self.toTimestamp(t1)
        if t2 not in ["last", "next", None]:
            ts2 = self.toTimestamp(t2)
        out = {}

        # Build variable list
        variables = self.getVariableSet(pattern_or_list)
        if len(variables) == 0:
            self._log.warning("No variables found.")
            return {}
        else:
            logvars = []
            for jvar in variables:
                logvars.append(jvar.toString())
            self._log.info(
                "List of variables to be queried: {0}".format(
                    ", ".join(logvars)
                )
            )

        if fundamentals is not None and ts2 is None:
            self._log.warning(
                "Unsupported: if filtering by fundamentals "
                "you must provide a correct time window"
            )
            return {}

        # Acquire
        all_ds_empty = True # Check if all datasets are empty, if yes then return {}
        for jvar in variables:
            if t2 is None or t2 == "last":
                res = [
                    self._ts.getLastDataPriorToTimestampWithinDefaultInterval(
                        jvar, ts1
                    )
                ]
                if res[0] is None:
                    res = []
                    datatype = None
                else:
                    datatype = res[0].getVariableDataType().toString()
                    self._log.info(
                        "Retrieved {0} values for {1}".format(
                            1, jvar.getVariableName()
                        )
                    )
            elif t2 == "next":
                res = [
                    self._ts.getNextDataAfterTimestampWithinDefaultInterval(
                        jvar, ts1
                    )
                ]
                if res[0] is None:
                    res = []
                    datatype = None
                else:
                    datatype = res[0].getVariableDataType().toString()
                    self._log.info(
                        "Retrieved {0} values for {1}".format(
                            1, jvar.getVariableName()
                        )
                    )
            else:
                if fundamentals is not None:
                    res = self._get_timeseries_data_filtered_by_fundamentals(jvar, ts1, ts2, fundamentals)
                else:
                    res = self._ts.getDataInTimeWindow(jvar, ts1, ts2)

                if res:
                    datatype = res.getVariableDataType().toString()
                    self._log.info(
                        "Retrieved {0} values for {1}".format(
                            res.size(), jvar.getVariableName()
                        )
                    )

            if (res):
                out[jvar.getVariableName()] = self.processDataset(res, datatype, unixtime)
                all_ds_empty = False
            else:
                out[jvar.getVariableName()] = np.array([], dtype=float), np.array([], dtype=float)

        if all_ds_empty:
            return {}
        return out

    def getVariable(
        self, variable, t1, t2=None, fundamental=None, unixtime=True
    ):
        return self.get(variable, t1, t2, fundamental, unixtime)[variable]

    def getScaled(
        self,
        pattern_or_list,
        t1,
        t2,
        unixtime=True,
        scaleAlgorithm="SUM",
        scaleInterval="MINUTE",
        scaleSize="1",
    ):
        """Query the database for a list of variables or for variables whose
        name matches a pattern (string) in a time window from t1 to t2.

        If no pattern if given for the fundamental all the data are returned.

        If a fundamental pattern is provided, the end of the time window as to
        be explicitely provided.

        Applies the scaling with supplied scaleAlgorithm, scaleSize, scaleInterval
        """
        ts1 = self.toTimestamp(t1)
        ts2 = self.toTimestamp(t2)
        timescaling = self.toTimescale(
            [scaleSize, scaleInterval, scaleAlgorithm]
        )

        out = {}
        # Build variable list
        variables = self.getVariableSet(pattern_or_list)
        if len(variables) == 0:
            self._log.warning("No variables found.")
            return {}
        else:
            logvars = []
            for jvar in variables:
                logvars.append(jvar.toString())
            self._log.info(
                "List of variables to be queried: {0}".format(
                    ", ".join(logvars)
                )
            )

        # Acquire
        for jvar in variables:
            try:
                res = self._ts.getDataInFixedIntervals(
                    jvar, ts1, ts2, timescaling
                )
            except jpype.JException as e:
                print(e.message())
                print(
                    """
                   scaleAlgorithm should be one of:{},
                   scaleInterval one of:{},
                   scaleSize an integer""".format(
                        [
                            "MAX",
                            "MIN",
                            "AVG",
                            "COUNT",
                            "SUM",
                            "REPEAT",
                            "INTERPOLATE",
                        ],
                        [
                            "SECOND",
                            "MINUTE",
                            "HOUR",
                            "DAY",
                            "WEEK",
                            "MONTH",
                            "YEAR",
                        ],
                    )
                )
                return
            datatype = res.getVariableDataType().toString()
            self._log.info(
                "Retrieved {0} values for {1}".format(
                    res.size(), jvar.getVariableName()
                )
            )
            vname = jvar.getVariableName()
            out[vname] = self.processDataset(res, datatype, unixtime)
        return out

    def getLHCFillData(self, fill_number=None, unixtime=True):
        """Gets times and beam modes for a particular LHC fill.
        Parameter fill_number can be an integer to get a particular fill or
        None to get the last completed fill.
        """
        if isinstance(fill_number, int):
            data = self._FillService.getLHCFillAndBeamModesByFillNumber(
                fill_number
            )
        else:
            data = self._FillService.getLastCompletedLHCFillAndBeamModes()

        if data is None:
            return None
        else:
            return {
                "fillNumber": data.getFillNumber(),
                "startTime": self.fromTimestamp(data.getStartTime(), unixtime),
                "endTime": self.fromTimestamp(data.getEndTime(), unixtime),
                "beamModes": [
                    {
                        "mode": mode.getBeamModeValue().toString(),
                        "startTime": self.fromTimestamp(
                            mode.getStartTime(), unixtime
                        ),
                        "endTime": self.fromTimestamp(
                            mode.getEndTime(), unixtime
                        ),
                    }
                    for mode in data.getBeamModes()
                ],
            }

    def getLHCFillsByTime(self, t1, t2, beam_modes=None, unixtime=True):
        """Returns a list of the fills between t1 and t2.
        Optional parameter beam_modes allows filtering by beam modes.
        """
        ts1 = self.toTimestamp(t1)
        ts2 = self.toTimestamp(t2)

        BeamModeValue = self._BeamModeValue

        if beam_modes is None:
            fills = self._FillService.getLHCFillsAndBeamModesInTimeWindow(
                ts1, ts2
            )
        else:
            if isinstance(beam_modes, str):
                beam_modes = beam_modes.split(",")

            valid_beam_modes = [
                mode
                for mode in beam_modes
                if BeamModeValue.isBeamModeValue(mode)
            ]

            if len(valid_beam_modes) == 0:
                raise ValueError("no valid beam modes found")

            java_beam_modes = BeamModeValue.parseBeamModes(
                ",".join(valid_beam_modes)
            )

            fills = self._FillService.getLHCFillsAndBeamModesInTimeWindowContainingBeamModes(
                ts1, ts2, java_beam_modes
            )

        return [
            self.getLHCFillData(fill, unixtime)
            for fill in fills.getFillNumbers()
        ]

    def getIntervalsByLHCModes(
        self,
        t1,
        t2,
        mode1,
        mode2,
        unixtime=True,
        mode1time="startTime",
        mode2time="endTime",
        mode1idx=0,
        mode2idx=-1,
    ):
        """Returns a list of the fill numbers and interval between t1 and
        t2 between the startTime of first beam mode in mode1 and the
        endTime of the first beam mode.

        The optional parameters 'mode[12]time' can take
        'startTime' or 'endTime'

        The otional parameter 'mode[12]idx' selects which mode in case of
        multiple occurrence of mode

        """
        ts1 = self.toTimestamp(t1)
        ts2 = self.toTimestamp(t2)
        fills = self.getLHCFillsByTime(ts1, ts2, [mode1, mode2])
        out = []
        for fill in fills:
            fn = fill["fillNumber"]
            m1 = []
            m2 = []
            for bm in fill["beamModes"]:
                if bm["mode"] == mode1:
                    m1.append(bm[mode1time])
                if bm["mode"] == mode2:
                    m2.append(bm[mode2time])
            if len(m1) > 0 and len(m2) > 0:
                out.append([fn, m1[mode1idx], m2[mode2idx]])
        return out

    def getMetaData(self, pattern_or_list):
        """Get All MetaData for a variable defined by a pattern_or_list"""
        out = {}
        variables = self.getVariableSet(pattern_or_list)
        for jvar in variables:

            vector_elements = self._md.getVectorElements(jvar)
            if vector_elements is None:
                continue
            metadata = vector_elements.getVectornumericElements()

            ts = [_Timestamp2float(tt) for tt in metadata]
            #            vv=[dict([(aa.key,aa.value) for aa in a.iterator()])
            #                    for a in metadata.values()]
            vv = [
                [aa.getValue() for aa in a.iterator()]
                for a in metadata.values()
            ]
            out[jvar.getVariableName()] = ts, vv
        return out

    def getVariablesOrigin(self, pattern_or_list):
        result = {}

        variables = self.getVariableSet(pattern_or_list)
        for var in list(variables.getVariables()):
            result[var.getVariableName()] = var.getSystem()

        return result

    def _getHierarchies(self, pattern_or_list: Union[str, List[str], Tuple[str]],
                        system: str = DEFAULT_HIERARCHY_NXCALS_SYSTEM) -> Set[Any]:
        hierarchies = self._nxcals_metadata_api.queries.Hierarchies
        if isinstance(pattern_or_list, str):
            hierarchies = self._hierarchyService.findAll(
                hierarchies.suchThat().systemName().eq(system).and_().path().like(pattern_or_list))
        elif isinstance(pattern_or_list, (list, tuple)):
            hierarchies = self._hierarchyService.findAll(
                hierarchies.suchThat().systemName().eq(system).and_().path().in_(self.toStringList(pattern_or_list)))
        else:
            raise ValueError(f"{pattern_or_list} not pattern or list")
        return hierarchies

    def getChildrenForHierarchies(self, pattern_or_list: Union[str, List[str], Tuple[str]],
                                  system: str = DEFAULT_HIERARCHY_NXCALS_SYSTEM) -> Dict[str, List[str]]:
        """
        Get a list of children hierarchy paths for given parent hierarchy paths.
        Wildcard for the pattern is '%'.

        Args:
            pattern_or_list: pattern for the selection of parent node paths or a list of parent node paths
            system: under which hierarchies are defined (default CERN)
        Returns:
            Dictionary of hierarchy paths with corresponding list of children hierarchy paths
        """
        result = {}

        for h in self._getHierarchies(pattern_or_list, system):
            result[h.getNodePath()] = [c.getNodePath() for c in h.getChildren()]

        return result

    def getVariablesForHierarchies(self, pattern_or_list: Union[str, List[str], Tuple[str]],
                                   system: str = DEFAULT_HIERARCHY_NXCALS_SYSTEM) -> Dict[str, List[str]]:
        """
        Get a list of variables attached to hierarchies.
        Wildcard for the pattern is '%'.

        Args:
            pattern_or_list: pattern for the selection of hierarchy paths or a list of hierarchy paths
            system: under which hierarchies are defined (default CERN)
        Returns:
            Dictionary of hierarchy paths with corresponding list of attached variables
        """
        result = {}
        for h in self._getHierarchies(pattern_or_list, system):
            result[h.getNodePath()] = [v.getVariableName() for v in self._hierarchyService.getVariables(h.getId())]
        return result

    def getHierarchiesForVariables(self, pattern_or_list: Union[str, List[str], Tuple[str]]) -> Dict[str, List[str]]:
        """
        Get a list of hierarchy paths for variables (in which given variables are defined).
        Wildcard for the pattern is '%'.

        Args:
            pattern_or_list: pattern for the selection of variables names or a list of variables names
        Returns:
            Dictionary of variables with corresponding list of hierarchy paths
        """
        result = {}
        variables = self._nxcals_metadata_api.queries.Variables

        if isinstance(pattern_or_list, str):
            variables = self._variableService.findAll(variables.suchThat().variableName().like(pattern_or_list))
        elif isinstance(pattern_or_list, (list, tuple)):
            variables = self._variableService.findAll(
                variables.suchThat().variableName().in_(self.toStringList(pattern_or_list)))
        else:
            raise ValueError(f"{pattern_or_list} not pattern or list")

        for v in variables:
            result[v.getVariableName()] = \
                [h.getNodePath() for h in self._hierarchyService.getHierarchiesForVariable(v.getId())]

        return result

    def _get_snapshots(self, pattern_or_list: Union[str, List[str], Tuple[str]],
                       owner_pattern: str = None, description_pattern: str = None) -> List[Any]:

        groups = self._nxcals_metadata_api.queries.Groups
        group_type = self._nxcals_custom_api.domain.GroupType

        snap_condition = groups.suchThat().label().eq(str(group_type.SNAPSHOT)).and_().name()

        if isinstance(pattern_or_list, str):
            snap_condition = snap_condition.like(pattern_or_list)
        elif isinstance(pattern_or_list, (list, tuple)):
            snap_condition = snap_condition.in_(self.toStringList(pattern_or_list))

        if owner_pattern is not None:
            snap_condition = snap_condition.and_().ownerName().like(owner_pattern)

        if description_pattern is not None:
            snap_condition = snap_condition.and_().description().like(description_pattern)

        return self._groupService.findAll(snap_condition)

    def getSnapshotNames(self, pattern_or_list: Union[str, List[str], Tuple[str]],
                         owner_pattern: str = "%", description_pattern: str = "%") -> List[str]:
        """
        Get a list of snapshots names based on a list of strings or a pattern,
        filtered by owner pattern and description pattern.

        Wildcard for the pattern is '%'.

        Args:
            pattern_or_list: pattern for the selection of snapshot names or a list of snapshot names
            owner_pattern: pattern for the filtering by owner name
            description_pattern: pattern for the filtering by snapshot description

        Returns:
            A list of snapshot names
        """
        return [snapshot.getName() for
                snapshot in self._get_snapshots(pattern_or_list, owner_pattern, description_pattern)]

    def getVariablesForSnapshots(self, pattern_or_list: Union[str, List[str], Tuple[str]],
                                 owner_pattern: str = "%") -> Dict[str, List[str]]:
        """
        Get variables attached to snapshots.
        Wildcard for the pattern is '%'.

        Args:
            pattern_or_list: pattern for the selection of snapshot names or a list of snapshot names
            owner_pattern: pattern for the filtering by owner name

        Returns:
           Dictionary of snapshots with attached list of variable names
        """
        result = {}

        group_property_name = self._nxcals_custom_api.domain.GroupPropertyName
        snapshots = self._get_snapshots(pattern_or_list, owner_pattern)

        for snapshot in snapshots:
            variables = self._groupService.getVariables(
                snapshot.getId())[group_property_name.getSelectedVariables.toString()]
            if variables:
                result[snapshot.getName()] = [variable.getVariableName() for variable in variables]

        return result


    def _get_snapshot_properties(self, fillNumber: Optional[str] = None,
                                 isEndTimeDynamic: Optional[str] = None,
                                 getTimeZone: Optional[str] = None,
                                 beamModeStart: Optional[str] = None,
                                 beamModeEnd: Optional[str] = None,
                                 getPriorTime: Optional[str] = None,
                                 getTime: Optional[str] = None,
                                 getDynamicDuration: Optional[str] = None,
                                 getStartTime: Optional[str] = None,
                                 getEndTime: Optional[str] = None,
                                 fundamentalFilter: Optional[str] = None,
                                 **kwargs) -> Tuple[str, str, str]:
        """
        Please note that:
        fillNumber attribute is mutually exclusive with isEndTimeDynamic since they represent 2 different ways of
        specyfing the timew indow: "by fill" vs "dynamic". If both are not present, a search with a fixed time window
        will be performed.
        The fundamentalFilter attribute is independent of the search method.
        """

        # Note: accessing kwargs from this function is not encouraged in order to keep the signature readable.
        _ = kwargs

        if fillNumber is not None:

            # Fills
            t1 = _adjust_timestamp_format(json.loads(beamModeStart)['validity']['startTime'])
            t2 = _adjust_timestamp_format(json.loads(beamModeEnd)['validity']['endTime'])

        elif isEndTimeDynamic is not None:
            if isEndTimeDynamic == "true":
                now = datetime.datetime.now()
                if getTimeZone == 'UTC_TIME':
                    now = datetime.datetime.utcnow()

                t1, t2 = self._get_timewindow_from_snapshot_attributes(
                    now,
                    getPriorTime,
                    getTime,
                    int(getDynamicDuration)
                )
            else:
                # Fixed
                t1 = LoggingDB._assure_local_time(getTimeZone, getStartTime)
                t2 = LoggingDB._assure_local_time(getTimeZone, getEndTime)
        else:
            raise ValueError("Neither \'fillNumber\' nor \'isEndTimeDynamic\' is present in the snapshot attributes")

        fundamental_pattern = None
        if fundamentalFilter is not None:
            fun_filter = json.load(fundamentalFilter)
            fundamental_pattern = ":".join(
                [fun_filter['accelerator'], fun_filter['lsaCycle'], fun_filter['timingUser']])

        return t1, t2, fundamental_pattern

    @staticmethod
    def _assure_local_time(time_zone, ts_str: str) -> str:
        if time_zone is not None and time_zone == 'UTC_TIME':
            utc_time = datetime.datetime.strptime(ts_str, "%Y-%m-%d %H:%M:%S.%f")
            local_tz = timezone(time.tzname[0])
            local_time = utc_time.astimezone(local_tz)
            return local_time.strftime("%Y-%m-%d %H:%M:%S.%f")
        return ts_str

    @staticmethod
    def _get_timewindow_from_snapshot_attributes(
            ref_time, prior_time: str, time_unit: str,
            dynamic_duration: int) -> (datetime.datetime, datetime.datetime):

        if prior_time == 'Now':
            t2 = ref_time
        elif prior_time == "Start of hour":
            t2 = ref_time.replace(microsecond=0, second=0, minute=0)
        elif prior_time == "Start of day":
            t2 = ref_time.replace(microsecond=0, second=0, minute=0, hour=0)
        elif prior_time == "Start of month":
            t2 = ref_time.replace(microsecond=0, second=0, minute=0, hour=0, day=1)
        elif prior_time == "Start of year":
            t2 = ref_time.replace(microsecond=0, second=0, minute=0, hour=0, day=1, month=1)
        else:
            raise ValueError(prior_time + " not supported as a prior time")

        # Dynamic timewindow
        args = {time_unit.lower(): dynamic_duration}
        t1 = t2 - relativedelta(**args)

        return t1, t2

    def getDataUsingSnapshots(self, pattern_or_list: Union[str, List[str], Tuple[str]],
                              owner_pattern: str = "%",
                              unixtime: bool = True,
                              search_criteria: SnapshotSearchCriteria = None) -> Dict[str, Dict[str, List[Tuple[Any]]]]:
        """
        Get data for variables attached to snapshots which are selected by provided list of strings or pattern
        in a time window defined in the snapshot configuration

        Args:
            pattern_or_list: pattern for the selection of snapshot names or a list of snapshot names
            owner_pattern: pattern for the filtering by owner name
            search_criteria: SnapshotSearchCriteria object having a triple of optional values:
                fundamental_pattern, for example: "CPS:%:SFTPRO%". If no pattern is given for the fundamental
                all the data are returned.
                start_time:
                end_time: if specified then the values from snapshot configuration will be overwritten
        Returns:
            A dictionary of snapshots names having dictionaries of variables
            with corresponding data for a given time period
        """
        t1 = 0
        t2 = 0
        fundamental_pattern = None

        if search_criteria:
            if search_criteria.fundamental_pattern:
                fundamental_pattern = search_criteria.fundamental_pattern

            if search_criteria.start_time:
                t1 = search_criteria.start_time

                if search_criteria.end_time:
                    t2 = search_criteria.end_time
                else:
                    t2 = None

        snapshot_list = self._get_snapshots(pattern_or_list, owner_pattern)

        result = {}
        for snapshot in snapshot_list:

            if not search_criteria:
                t1, t2, fundamental_pattern = self._get_snapshot_properties(**snapshot.getProperties())

            variables_for_snapshot = self.getVariablesForSnapshots([snapshot.getName()],
                                                                   snapshot.getOwner())[snapshot.getName()]
            result[snapshot.getName()] = self.get(variables_for_snapshot, t1, t2, fundamental_pattern, unixtime)

        return result
