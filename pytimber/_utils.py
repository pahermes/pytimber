try:
    # Pandas is an optional dependency, so import it safely, and provide a
    # function (_import_pandas) to get the pandas module or raise.
    import pandas as pd
except ImportError:
    pd = None


def _import_pandas():
    if pd is None:
        raise ImportError(
            "pandas must be installed in order to use this function"
        )
    return pd
