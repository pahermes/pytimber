from setuptools_scm import get_version
from packaging.version import parse

vn = parse(get_version(local_scheme="no-local-version"))
print(f'{vn.major}.{vn.minor}.{vn.micro}')