import pytimber

nxcals = pytimber.NXCals()

ds = (
    nxcals.DataQuery.byEntities()
    .system("CMW")
    .startTime("2018-04-29 00:00:00.000")
    .endTime("2018-04-30 00:00:00.000")
    .entity()
    .keyValue("device", "LHC.LUMISERVER")
    .keyValue("property", "CrossingAngleIP1")
    .buildDataset()
)

arr = nxcals.spark2numpy(ds)
